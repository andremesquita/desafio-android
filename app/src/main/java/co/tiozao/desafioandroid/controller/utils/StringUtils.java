package co.tiozao.desafioandroid.controller.utils;

import android.view.View;

/**
 * Created by andre on 4/5/17.
 */

public class StringUtils {

    public static String stringFromResource(int res, View view) {
        return view.getContext().getResources().getString(res);
    }
}
