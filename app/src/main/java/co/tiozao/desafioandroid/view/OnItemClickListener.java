package co.tiozao.desafioandroid.view;

import co.tiozao.desafioandroid.model.ShotModel;

/**
 * Created by andre on 4/4/17.
 */

public interface OnItemClickListener {
    void onItemClick(ShotModel item);
}
