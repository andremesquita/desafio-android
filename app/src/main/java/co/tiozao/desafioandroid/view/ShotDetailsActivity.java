package co.tiozao.desafioandroid.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import butterknife.BindView;
import butterknife.ButterKnife;
import co.tiozao.desafioandroid.R;
import co.tiozao.desafioandroid.controller.utils.StringUtils;
import co.tiozao.desafioandroid.model.ShotModel;

/**
 * Created by andre on 4/4/17.
 */

public class ShotDetailsActivity extends AppCompatActivity {

    public static final String SHOT_EXTRA_BUNDLE_IDENTIFIER = "SHOT_EXTRA_BUNDLE_IDENTIFIER";

    @BindView(R.id.description)
    TextView description;

    @BindView(R.id.author)
    TextView author;

    @BindView(R.id.views)
    TextView views;

    @BindView(R.id.comments)
    TextView comments;

    @BindView(R.id.image)
    ImageView image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shot_details);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Intent i = getIntent();
        ShotModel shotModel = (ShotModel)i.getSerializableExtra(SHOT_EXTRA_BUNDLE_IDENTIFIER);

        if(shotModel != null) {
            setShot(shotModel);
        }
    }

    void setShot (final ShotModel shot) {
        if(shot.description != null) {
            description.setText(Html.fromHtml(shot.description));
        }

        if(shot.user != null && shot.user.name != null) {
            author.setText(shot.user.name);
        }

        image.setImageDrawable(null);

        if(shot.images != null && shot.images.teaser != null) {
            ImageLoader imageLoader = ImageLoader.getInstance();

            imageLoader.displayImage(shot.images.teaser, image);
        }

        views.setText(
                String.format(
                        StringUtils.stringFromResource(R.string.label_views, views), shot.views_count));
        comments.setText(
                String.format(
                        StringUtils.stringFromResource(R.string.label_comments, comments), shot.comments_count));
    }
}
