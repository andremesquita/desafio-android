package co.tiozao.desafioandroid.view.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import butterknife.BindView;
import butterknife.ButterKnife;
import co.tiozao.desafioandroid.R;
import co.tiozao.desafioandroid.controller.utils.StringUtils;
import co.tiozao.desafioandroid.model.ShotModel;

/**
 * Created by andre on 4/4/17.
 */

public class ShotHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.date)
    TextView date;

    @BindView(R.id.views)
    TextView views;

    @BindView(R.id.comments)
    TextView comments;

    @BindView(R.id.image)
    ImageView image;

    View view;

    public ShotHolder(View view) {
        super(view);
        ButterKnife.bind(this, view);

        this.view = view;
    }

    public void setShot(ShotModel shot) {
        image.setImageDrawable(null);

        if(shot.images != null && shot.images.teaser != null) {
            ImageLoader imageLoader = ImageLoader.getInstance();

            imageLoader.displayImage(shot.images.normal, image);
        }

        views.setText(
                String.format(
                        StringUtils.stringFromResource(R.string.label_views, views), shot.views_count));

        comments.setText(
                String.format(
                        StringUtils.stringFromResource(R.string.label_comments, comments), shot.comments_count));

        date.setText(StringUtils.stringFromResource(R.string.label_date, date) + shot.updated_at);
    }

    public TextView getDate() {
        return date;
    }

    public void setDate(TextView author) {
        this.date = author;
    }

    public ImageView getImage() {
        return image;
    }

    public void setImage(ImageView image) {
        this.image = image;
    }
}